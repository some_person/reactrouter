import React , {Fragment, Component} from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Container, Row, Col} from "reactstrap";

import Header from "./includes/header";
import Footer from "./includes/footer";
import Main from "./../pages/index";
import Page404 from "./../pages/404";
import Users from "./../pages/users";
import Private from "./../pages/private";
import AuthPage from "./../pages/forms/auth";
import UserInfo from "./../pages/userInfo";
import Gallery from "./../pages/userGallery";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuth: false
        };
    }

    render() {
        let {isAuth} = this.state;
        return(
            <Fragment>
                <Router>
                    <Header isAuth={isAuth}/>
                    <Container>
                        <Switch>
                            <Route exact path="/" component={Main}/>

                            <Route exact path={"/users"} component={Users} />

                            {isAuth ? <Route exact path={"/private"} component={()=><Private logout={this.toggleAuth}/>} /> :
                                <Route exact path={"/private"} component={()=><AuthPage auth={this.toggleAuth}/>} /> }

                            <Route exact path={"/user/:id"} component={UserInfo}/>

                            <Route exact path={"/gallery/:id"} component={Gallery}/>

                            <Route component={Page404}/>
                        </Switch>
                    </Container>
                    <Footer/>
                </Router>
            </Fragment>
        )
    }

    toggleAuth = (type) => {
        this.setState({isAuth: type === true})
    }
}