import "./scss/style.scss";
import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle, Badge, CardImg,
    Container, Row, Col} from "reactstrap";
import {Link} from "react-router-dom";

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuActive: false
        }
    }

    render() {
        let {isAuth} = this.props;
        return (
            <Fragment>
                <header className="header">
                    <Container>
                        <Row className="header__body">
                            <div className={this.state.menuActive ? 'header__burger active' : 'header__burger'} onClick={this.toggleMenu}>
                                <span></span>
                            </div>
                            <Col lg="8" className={this.state.menuActive ? 'header__menu active' : 'header__menu'}>
                                <ul className="header__list">
                                    <li>
                                        <Link to="/" className="header__link">Home</Link>
                                    </li>
                                    <li>
                                        <Link to="/users" className="header__link">Users</Link>
                                    </li>
                                    <li>
                                        <Link to="/private" className="header__link">{!isAuth ? "Log in" : "Private"}</Link>
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </Container>
                </header>
            </Fragment>
        )
    }

    toggleMenu = () => {
        this.setState({
            menuActive: !this.state.menuActive
        })
    }
}