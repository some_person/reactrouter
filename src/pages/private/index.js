import React , {Fragment, Component} from "react";

export default class Users extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <Fragment>
                <h1>Private</h1>
                <p className="logout" onClick={() => this.logout()}>Logout</p>
            </Fragment>
        )
    }

    logout() {
        this.props.logout(false);
    }
}