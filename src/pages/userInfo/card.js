import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle, Badge,
Container, Row, Col} from "reactstrap";

import {Link} from "react-router-dom";

export default class CardItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {user} = this.props;
        return (
            <Fragment>
                <h1>#{user.id} {user.name}</h1>
                <Card>
                    <CardBody>
                        <Row className="userInfo">
                            <Col lg={12}><Badge color="info">Name:</Badge> {user.name}</Col>
                            <Col lg={12}><Badge color="info">Username:</Badge> {user.username}</Col>
                            <Col lg={12}><Badge color="info">Email:</Badge> {user.email}</Col>
                            <Col lg={12}>
                                <Badge color="info">Address:</Badge>
                                <Col lg={12}>
                                    <Badge color="primary">Street:</Badge> {user.address.street}
                                </Col>
                                <Col lg={12}>
                                    <Badge color="primary">Suite:</Badge> {user.address.suite}
                                </Col>
                                <Col lg={12}>
                                    <Badge color="primary">City:</Badge> {user.address.city}
                                </Col>
                                <Col lg={12}>
                                    <Badge color="primary">Zipcode:</Badge> {user.address.zipcode}
                                </Col>
                                <Col lg={12}>
                                    <Badge color="primary">Geo:</Badge>
                                    <Col lg={12}>
                                        <Badge color="success">Lat:</Badge> {user.address.geo.lat}
                                    </Col>
                                    <Col lg={12}>
                                        <Badge color="success">Lng:</Badge> {user.address.geo.lng}
                                    </Col>
                                </Col>
                            </Col>
                            <Col lg={12}><Badge color="info">Phone:</Badge> {user.phone}</Col>
                            <Col lg={12}><Badge color="info">Website:</Badge> {user.website}</Col>
                            <Col lg={12}><Badge color="info">Company:</Badge> {user.company.name}</Col>
                        </Row>
                    </CardBody>
                </Card>
            </Fragment>
        )
    }
}