import React , {Fragment, Component} from "react";
import {Spinner} from "reactstrap/dist/reactstrap.es";
import {Card, CardBody, CardTitle, CardSubtitle, Badge,
Container, Row, Col} from "reactstrap";
import {Link} from "react-router-dom";
import CardItem from './card';

export default class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            albums: []
        }

    }

    render() {
        let {user, albums} = this.state;
        return (
            user ?
            <Fragment>
                <CardItem user={user}/>
                <h4>Albums</h4>
                {albums.length?
                    albums.map(album => {
                        return <Card key={album.id}>
                            <CardBody>
                                <CardTitle className="card__title">
                                    <Link className="card__link" to={`/gallery/${album.id}`}>#{album.id} {album.title}</Link>
                                </CardTitle>
                            </CardBody>
                        </Card>
                    }):
                    <Spinner color="success"/>
                }
            </Fragment> :
            <Spinner color="dark"/>
        )
    }

    componentDidMount() {
        let userId = this.props.match.params.id;

        fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
            .then(user => user.json())
            .then(user => {
                this.setState({user: Object.assign({}, user)})
                return user
            })
            .then(user => fetch(`https://jsonplaceholder.typicode.com/users/${user.id}/albums`))
            .then(albums => albums.json())
            .then(albums => this.setState({albums: [...albums]}))
    }
}