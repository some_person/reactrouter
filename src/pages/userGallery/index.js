import React , {Fragment, Component} from "react";
import {Spinner} from "reactstrap/dist/reactstrap.es";
import {Card, CardBody, CardTitle, CardSubtitle, Badge, CardImg,
Container, Row, Col,
Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";


export default class Gallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photos: [],
            showModalImg: false,
            currentImg: {}
        }

    }

    render() {
        let {photos, showModalImg, currentImg} = this.state;
        let albumID = this.props.match.params.id;
        return (
            photos.length?
                <Fragment>
                    <Container className="gallery">
                        <Row>
                            <Col lg={12}>
                                <h2 className="gallery__header">
                                    Album #{albumID} gallery
                                </h2>
                            </Col>
                        </Row>
                        <Row>
                            {photos.map(photo => {
                                return <Col key={photo.id} lg="2" md="4" xs="12" sm="6" className="item-gallery">
                                    <Col lg="12" className="item-gallery__container">
                                        <Col lg={12} className="item-gallery__img" onClick={(e) => this.toggleModalImg(e, photo)}>
                                            <img src={photo.thumbnailUrl} alt={photo.title}/>
                                        </Col>
                                        <Col lg={12} className="item-gallery__title">
                                            <p>#{photo.id} {photo.title}</p>
                                        </Col>
                                    </Col>
                                </Col>

                            })}
                        </Row>
                    </Container>
                        <Modal isOpen={showModalImg} toggle={this.toggleModalImg} className="modalImg">
                            <ModalHeader className="item-gallery__title" toggle={this.toggleModalImg}>
                                {currentImg.title}
                            </ModalHeader>
                            <ModalBody>
                                <Container>
                                    <Row>
                                        <Col lg="12" className="modalImg__image-container">
                                            <img src={currentImg.url} alt={currentImg.title}/>
                                        </Col>
                                    </Row>
                                </Container>
                            </ModalBody>
                        </Modal>
                </Fragment>:
                <Spinner color="success"/>
        )
    }

    componentDidMount() {
        let id = this.props.match.params.id;

        fetch(`https://jsonplaceholder.typicode.com/albums/${id}/photos`)
            .then(photos => photos.json())
            .then(photos => this.setState({photos: [...photos]}))
    }

    toggleModalImg = (e, photo) => {
        if(!this.state.showModalImg) {
            let currentImg = {
                "url": photo.url,
                "title": photo.title
            }
            this.setState({
                showModalImg: !this.state.showModalImg,
                currentImg: Object.assign({}, currentImg)
            })
        } else {
            this.setState({
                showModalImg: !this.state.showModalImg,
            })
        }
    }
}