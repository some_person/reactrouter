import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle, Badge} from "reactstrap";
import { Spinner } from 'reactstrap';
import CardItem from './card';


export default class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    render() {
        let {users} = this.state;
        return (
            users.length ?
            <Fragment>
                <h2 className="page__title">User list</h2>
                {users.map(user => {
                    return <CardItem key={user.id} user={user}/>
                })}
            </Fragment> :
            <Spinner color="dark"/>
        )
    }

    componentDidMount(){
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(users => users.json())
            .then(users => {
                this.setState({users: [...users]});
            })
    }
}