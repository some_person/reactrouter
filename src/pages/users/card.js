import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle, Badge,
Container, Row, Col} from "reactstrap";

import {Link} from "react-router-dom";

export default class CardItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {user} = this.props;
        return (
            <Fragment>
                <Card>
                    <CardBody>
                        <CardTitle className="card__title">
                            <Link className="card__link" to={`/user/${user.id}`}>#{user.id} {user.name}</Link>
                        </CardTitle>

                        <CardTitle className="card__subtitle">
                            <Badge color="info">username:</Badge> {user.username}
                        </CardTitle>
                    </CardBody>
                </Card>
            </Fragment>
        )
    }
}