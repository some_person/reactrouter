import React , {Fragment, Component} from "react";
import {Label ,Form,Input, Button,FormGroup, FormText} from "reactstrap";

export default class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorText: ""
        }
    }

    render() {
        let {errorText} = this.state;
        return (
            <Fragment>
                <div className="formAuth">
                    <h3 className="formAuth__title">Authentication</h3>
                    <Form onSubmit={(e) => this.formSubmit(e)} className="formAuth__form">
                        <FormGroup>
                            <Label for="login">login</Label>
                            <Input type="login" name="login" id="login" placeholder="login"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">password</Label>
                            <Input type="password" name="password" id="password" placeholder="password"/>
                            <FormText className={errorText.length ? "error_show" : "error_hide"}>{errorText}</FormText>
                        </FormGroup>
                        <Button>Submit</Button>
                    </Form>
                </div>
            </Fragment>
        )
    }

    formSubmit = (e) => {
        e.preventDefault();
        let formData = new FormData(e.target);
        let isValid = formData.get("login") === "login" && formData.get("password") === "123";
        let empty = !formData.get("login").trim().length || !formData.get("password").trim().length;
        if (!empty && isValid) {
            this.setState({
                errorText: ""
            });
            this.props.auth(true);
        } else if(empty) {
            this.setState({
                errorText: "Fields are required!",
            })
        }
        else {
            this.setState({
                errorText: "Wrong login or password!",
            })
        }
    }
}